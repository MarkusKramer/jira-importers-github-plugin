package com.atlassian.jira.plugins.importer.github;

import com.atlassian.jira.plugins.importer.github.config.ConfigBean;
import com.atlassian.jira.plugins.importer.github.fetch.DataFetchJob;
import com.atlassian.jira.plugins.importer.github.fetch.GithubDataService;
import com.atlassian.jira.plugins.importer.github.importer.DataBean;
import com.atlassian.jira.plugins.importer.github.web.AuthenticationPage;
import com.atlassian.jira.plugins.importer.github.web.FetchDataPage;
import com.atlassian.jira.plugins.importer.github.web.LabelMappingPage;
import com.atlassian.jira.plugins.importer.github.web.WorkflowPage;
import com.atlassian.jira.plugins.importer.imports.importer.ImportDataBean;
import com.atlassian.jira.plugins.importer.imports.importer.JiraDataImporter;
import com.atlassian.jira.plugins.importer.web.AbstractImporterController;
import com.atlassian.jira.plugins.importer.web.AbstractSetupPage;
import com.atlassian.jira.plugins.importer.web.ConfigFileHandler;
import com.atlassian.jira.plugins.importer.web.ImporterProjectMappingsPage;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Controls the GitHub import.
 */
public class GithubImporterController extends AbstractImporterController {

    public static final String IMPORT_CONFIG_BEAN = "com.atlassian.jira.plugins.importer.github.config";
    public static final String IMPORT_ID = "com.atlassian.jira.plugins.importer.github.import";
    public static final String EXTERNAL_SYSTEM = "com.atlassian.jira.plugins.jira-importers-github-plugin:githubImporterKey";

    private final ConfigFileHandler configFileHandler;

    public GithubImporterController(JiraDataImporter importer, ConfigFileHandler configFileHandler) {
        super(importer, IMPORT_CONFIG_BEAN, IMPORT_ID);
        this.configFileHandler = configFileHandler;
    }

    @Override
    public boolean createImportProcessBean(AbstractSetupPage abstractSetupPage) {
        GithubDataService githubDataService = new GithubDataService();
        DataFetchJob dataFetchJob = new DataFetchJob(githubDataService);
        ConfigBean configBean = new ConfigBean(githubDataService);

        storeImportProcessBeanInSession(new GithubImportProcessBean(githubDataService, dataFetchJob, configBean));
        return true;
    }

    @Override
    public ImportDataBean createDataBean() throws Exception {
        GithubImportProcessBean importProcessBean = (GithubImportProcessBean) getImportProcessBeanFromSession();
        if( importProcessBean == null ) {
            throw new IllegalStateException("No active importer");
        }

        ConfigBean configBean = (ConfigBean) importProcessBean.getConfigBean();
        return new DataBean(configBean, importProcessBean);
    }

    // @Override no override annotation to remain compatible with JIRA 5
    public List<String> getStepNameKeys() {
        return Lists.newArrayList(
                "com.atlassian.jira.plugins.importer.github.step.oauth",
                "com.atlassian.jira.plugins.importer.github.step.projectMapping",
                "com.atlassian.jira.plugins.importer.github.step.fetchData",
                "com.atlassian.jira.plugins.importer.github.step.labelMapping",
                "com.atlassian.jira.plugins.importer.github.step.workflow"
        );
    }

    @Override
    public List<String> getSteps() {
        return Lists.newArrayList(
                AuthenticationPage.class.getSimpleName(),
                ImporterProjectMappingsPage.class.getSimpleName(),
                FetchDataPage.class.getSimpleName(),
                LabelMappingPage.class.getSimpleName(),
                WorkflowPage.class.getSimpleName()
        );
    }

}
