package com.atlassian.jira.plugins.importer.github.importer;

import com.atlassian.jira.plugins.importer.external.beans.ExternalAttachment;
import com.atlassian.jira.plugins.importer.external.beans.ExternalIssue;
import com.atlassian.jira.plugins.importer.github.fetch.RemoteCall;
import com.atlassian.jira.plugins.importer.github.util.GithubRequestService;
import com.atlassian.jira.plugins.importer.github.util.HttpClientFactory;
import com.atlassian.jira.plugins.importer.imports.importer.ImportLogger;
import com.atlassian.jira.util.AttachmentUtils;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AttachmentManager {

    private final Map<ExternalIssue,Set<Attachment>> issueAttachments = new HashMap<ExternalIssue, Set<Attachment>>();

    /**
     * Converts links to external resources (not issues) in markdown into JIRA's format.
     * If it is an attachment (a resource stored on GitHub) it is imported as well and the link changed.
     * @param externalIssue used to identify to which issue the attachments belong to
     */
    public String convertMarkdownLinks(String content, ExternalIssue externalIssue, ImportLogger importLogger) {
        Set<Attachment> attachments = issueAttachments.get(externalIssue);
        if( attachments == null ) {
            attachments = new HashSet<Attachment>();
            issueAttachments.put(externalIssue, attachments);
        }

        return convertMarkdownLinks(content, attachments);
    }

    protected String convertMarkdownLinks(String content, Set<Attachment> attachments) {
        // regex search for external links. links appear as:
        // [landscape_7](https://f.cloud.github.com/assets/1502825/389403/746768ca-a706-11e2-829c-b88fb39fb0b8.jpg) and
        // ![landscape_7](https://f.cloud.github.com/assets/1502825/389403/746768ca-a706-11e2-829c-b88fb39fb0b8.jpg "bla bla bla")
        Pattern pattern = Pattern.compile("(!?)\\[(.*?)\\]\\((.*?)(\\s+\\\".*?\\\")?\\)"); // (!?)\[(.*?)\]\((.*?)(\s+\".*?\")?\)

        Matcher matcher = pattern.matcher(content);
        while( matcher.find() ) {
            // extract link infos
            String entireMatch = matcher.group();
            boolean isEmbeddedContent = matcher.group(1).equals("!");
            String linkId = matcher.group(2);
            String linkTarget = matcher.group(3);

            // JIRA wiki markup format !jira.gif! (see https://jira.atlassian.com/browse/JRA-3669)
            // requires that linkId is a proper file name (i.e. add a file extension)
            if( isEmbeddedContent ) {
                String fileType = linkTarget.substring( linkTarget.lastIndexOf(".") );
                linkId = linkId + fileType;
            }

            // decide whether to download content as attachment
            boolean isGithubAttachment = linkTarget.contains("://f.cloud.github.com/assets");
            boolean importAttachment = isGithubAttachment && isEmbeddedContent;
            if(importAttachment) {
                // duplicates will be filtered out with this set (but not if a different linkId was used)
                attachments.add(new Attachment(linkId, linkTarget));
            }

            // replace link markup
            String replacement;
            if(isEmbeddedContent) {
                if( importAttachment ) {
                    replacement = "!"+linkId+"|thumbnail!";
                } else {
                    replacement = "!"+linkTarget+"|thumbnail!";
                }
            } else {
                replacement = linkTarget;
            }
            content = content.replace(entireMatch, replacement);
        }

        return content;
    }

    public List<ExternalAttachment> downloadAttachmentsForIssue(ExternalIssue externalIssue, ImportLogger importLogger) {
        Collection<Attachment> attachments = issueAttachments.get(externalIssue);
        List<ExternalAttachment> externalAttachments = new ArrayList<ExternalAttachment>();

        if( attachments != null ) {
            for( Attachment attachment : attachments ) {
                importLogger.log("Downloading attachment "+attachment.getFilename()+" from "+attachment.getDownloadUrl());
                Download download = null;
                try {
                    download = downloadFile(attachment.getDownloadUrl(), importLogger);
                } catch (IOException e) {
                    throw new RuntimeException("Giving up to download attachments", e);
                }

                if( download != null ) {
                    ExternalAttachment externalAttachment = new ExternalAttachment(attachment.getFilename(), download.getFile(), download.getLastModified());
                    externalAttachments.add(externalAttachment);
                } else {
                    importLogger.warn("Unable to download "+attachment.getDownloadUrl());
                }
            }
        }

        return externalAttachments;
    }

    private Download downloadFile(final String url, final ImportLogger importLogger) throws IOException {
        GithubRequestService requestService = new GithubRequestService(new GithubRequestService.FailureCallback() {
            @Override
            public void retry(String msg) {
                importLogger.warn(msg);
            }
            @Override
            public void retriesFinished() {
                importLogger.log("Successfully downloaded " + url + " after retries");
            }
        }, 10, null);

        try {
            return requestService.submit(new DownloadRemoteCall(url));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private static class DownloadRemoteCall implements RemoteCall<Download> {
        private final Logger log = Logger.getLogger(this.getClass());
        private String url;

        private DownloadRemoteCall(String url) {
            this.url = url;
        }

        @Override
        public Download call() throws IOException {
            Download download = new Download();

            HttpGet request = new HttpGet(url);
            HttpResponse response = HttpClientFactory.createHttpClient().execute(request);
            if( response.getStatusLine().getStatusCode() == HttpStatus.SC_OK ) {
                String lastModifiedStr = response.getFirstHeader(HttpHeaders.LAST_MODIFIED).getValue();
                try {
                    download.setLastModified( DateUtils.parseDate(lastModifiedStr) );
                } catch (DateParseException e) {
                    log.error(e);
                    download.setLastModified(new Date());
                }

                InputStream input = response.getEntity().getContent();
                File file = getTempFile();
                OutputStream output = new FileOutputStream(file);
                IOUtils.copy(input, output);
                input.close();
                output.close();
                download.setFile(file);

                return download;
            } else {
                return null;
            }
        }

        private File getTempFile() throws IOException {
            File tempDir = AttachmentUtils.getTemporaryAttachmentDirectory();
            File file = File.createTempFile("githubAttachment-", ".tmp", tempDir);

            if (file.getParentFile() != null) {
                FileUtils.forceMkdir(file.getParentFile());
            }

            return file;
        }
    }

    private static class Download {
        private File file;
        private Date lastModified;

        public File getFile() {
            return file;
        }

        public Date getLastModified() {
            return lastModified;
        }

        public void setFile(File file) {
            this.file = file;
        }

        public void setLastModified(Date lastModified) {
            this.lastModified = lastModified;
        }
    }

}
