package com.atlassian.jira.plugins.importer.github.fetch;

/**
 * GitHub issue management constants.
 */
public class GithubConstants {

    public static final String GITHUB_ISSUE_STATE = "state";
    public static final String GITHUB_ISSUE_STATE_OPEN = "open";
    public static final String GITHUB_ISSUE_STATE_CLOSED = "closed";

    public static final String GITHUB_MILESTONE_STATE_OPEN = "open";
    public static final String GITHUB_MILESTONE_STATE_CLOSED = "closed";

}
