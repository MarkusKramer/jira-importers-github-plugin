package com.atlassian.jira.plugins.importer.github.web;

import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.plugins.importer.github.config.ConfigBean;
import com.atlassian.jira.plugins.importer.github.config.SchemeStatusMapping;
import com.atlassian.jira.plugins.importer.github.rest.workflow.WorkflowResource;
import com.atlassian.jira.plugins.importer.github.rest.workflow.WorkflowService;
import com.atlassian.jira.plugins.importer.tracking.UsageTrackingService;
import com.atlassian.jira.plugins.importer.web.AbstractSetupPage;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.WebInterfaceManager;
import org.codehaus.jackson.map.ObjectMapper;
import webwork.action.ActionContext;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Allows the user select a workflow scheme and map the GitHub issue state to appropriate workflow statuses.
 */
public class WorkflowPage extends AbstractSetupPage {

    public static final String SCHEME_SELECT_ID = "workflowSchemeSelect";
    private final WorkflowService workflowService;

    public WorkflowPage(UsageTrackingService usageTrackingService, WebInterfaceManager webInterfaceManager, PluginAccessor pluginAccessor, WorkflowManager workflowManager, WorkflowSchemeManager workflowSchemeManager, ConstantsManager constantsManager) {
        super(usageTrackingService, webInterfaceManager, pluginAccessor);
        workflowService = new WorkflowService(workflowManager, workflowSchemeManager, constantsManager);
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        if( isNextClicked() ) {
            populateStatusMappings();
        }

        return super.doExecute();
    }

    @Override
    protected void doValidation() {
        if( isNextClicked() ) {
            Map<String,String> actionParams = ActionContext.getSingleValueParameters();
            String workflowSchemeName = actionParams.get(SCHEME_SELECT_ID);
            if( StringUtils.isBlank(workflowSchemeName) ) {
                addError(SCHEME_SELECT_ID, getText("com.atlassian.jira.plugins.importer.github.workflow.error.noScheme"));
                getConfigBean().setSchemeStatusMapping(null);
            }
        }
        super.doValidation();
    }

    private void populateStatusMappings() {
        Map<String,String> actionParams = ActionContext.getSingleValueParameters();

        String workflowSchemeName = actionParams.get(SCHEME_SELECT_ID);
        if( WorkflowResource.DEFAULT_WORKFLOW_SCHEME_KEY.equals(workflowSchemeName) ) {
            workflowSchemeName = null;
        }

        Collection<JiraWorkflow> workflows = workflowService.getSchemeWorkflows(workflowSchemeName);

        SchemeStatusMapping schemeStatusMapping = new SchemeStatusMapping(workflowSchemeName);
        for( JiraWorkflow workflow : workflows ) {
            String openStatus = actionParams.get(workflow.getName() + "_open_status");
            String closedStatus = actionParams.get(workflow.getName() + "_closed_status");

            SchemeStatusMapping.JiraStatusMapping statusMapping = new SchemeStatusMapping.JiraStatusMapping(openStatus,closedStatus);
            schemeStatusMapping.getWorkflowIdToStatusMapping().put(workflow.getName(), statusMapping);
        }

        getConfigBean().setSchemeStatusMapping(schemeStatusMapping);
    }

    @Override
    public String getFormTitle() {
        return getText("com.atlassian.jira.plugins.importer.github.workflow.title");
    }

    public Map<String, String> getWorkflowSchemeValues() {
        Map<String,String> values = new HashMap<String, String>();
        values.put(WorkflowResource.DEFAULT_WORKFLOW_SCHEME_KEY, workflowService.getDefaultSchemeName());

        for( String schemeName : workflowService.getSchemeNames() ) {
            values.put(schemeName, schemeName);
        }
        return values;
    }

    public String getSelectedWorkflowSchemeKey() {
        SchemeStatusMapping schemeStatusMapping = getConfigBean().getSchemeStatusMapping();
        if( schemeStatusMapping == null ) {
            return "";
        } else if( schemeStatusMapping.getWorkflowSchemeName() == null ) {
            return WorkflowResource.DEFAULT_WORKFLOW_SCHEME_KEY;
        } else {
            return schemeStatusMapping.getWorkflowSchemeName();
        }
    }

    public String getStatusMappingJSON() {
        Map<String, SchemeStatusMapping.JiraStatusMapping> workflowIdToStatusMapping;

        SchemeStatusMapping schemeStatusMapping = getConfigBean().getSchemeStatusMapping();
        if( schemeStatusMapping!= null) {
            workflowIdToStatusMapping = schemeStatusMapping.getWorkflowIdToStatusMapping();
        } else {
            workflowIdToStatusMapping = new HashMap<String, SchemeStatusMapping.JiraStatusMapping>();
        }

        try {
            return new ObjectMapper().writeValueAsString(workflowIdToStatusMapping);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public ConfigBean getConfigBean() {
        try {
            if (getController() == null || getController().getImportProcessBeanFromSession() == null) {
                throw new IllegalStateException("Importer not intialized");
            }
            ConfigBean configBean = (ConfigBean) getController().getImportProcessBeanFromSession().getConfigBean();
            return configBean;
        } catch (ClassCastException e) {
            throw new RuntimeException(e);
        }
    }

    public String getSchemeSelectId() {
        return SCHEME_SELECT_ID;
    }

}
