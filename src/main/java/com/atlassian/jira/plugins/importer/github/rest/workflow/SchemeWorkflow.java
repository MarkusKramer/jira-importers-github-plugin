package com.atlassian.jira.plugins.importer.github.rest.workflow;

import java.util.ArrayList;
import java.util.List;

/**
 * A workflow with its scheme dependent issue types and statuses.
 */
public class SchemeWorkflow {

    private String workflowName;
    private String workflowDescription;
    private List<JiraIssueType> associatedTypes;
    private List<JiraIssueStatus> statuses;

    public SchemeWorkflow() {
    }

    public SchemeWorkflow(String workflowName, String workflowDescription) {
        this.workflowName = workflowName;
        this.workflowDescription = workflowDescription;
        this.associatedTypes = new ArrayList<JiraIssueType>();
        this.statuses = new ArrayList<JiraIssueStatus>();
    }

    public String getWorkflowName() {
        return workflowName;
    }

    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    public String getWorkflowDescription() {
        return workflowDescription;
    }

    public void setWorkflowDescription(String workflowDescription) {
        this.workflowDescription = workflowDescription;
    }

    public List<JiraIssueType> getAssociatedTypes() {
        return associatedTypes;
    }

    public void setAssociatedTypes(List<JiraIssueType> associatedTypes) {
        this.associatedTypes = associatedTypes;
    }

    public List<JiraIssueStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<JiraIssueStatus> statuses) {
        this.statuses = statuses;
    }

    public static class JiraIssueType {
        private String id;
        private String name;
        private String iconUrl;

        public JiraIssueType() {
        }

        public JiraIssueType(String id, String name, String iconUrl) {
            this.id = id;
            this.name = name;
            this.iconUrl = iconUrl;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIconUrl() {
            return iconUrl;
        }

        public void setIconUrl(String iconUrl) {
            this.iconUrl = iconUrl;
        }
    }

    public static class JiraIssueStatus {
        private String id;
        private String name;

        public JiraIssueStatus() {
        }

        public JiraIssueStatus(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}