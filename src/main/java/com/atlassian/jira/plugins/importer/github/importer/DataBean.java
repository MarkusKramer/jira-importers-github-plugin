package com.atlassian.jira.plugins.importer.github.importer;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugins.importer.external.beans.*;
import com.atlassian.jira.plugins.importer.github.GithubImportProcessBean;
import com.atlassian.jira.plugins.importer.github.config.ConfigBean;
import com.atlassian.jira.plugins.importer.github.fetch.Project;
import com.atlassian.jira.plugins.importer.imports.importer.AbstractDataBean;
import com.atlassian.jira.plugins.importer.imports.importer.ImportLogger;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.eclipse.egit.github.core.Comment;
import org.eclipse.egit.github.core.Issue;
import org.eclipse.egit.github.core.Milestone;
import org.eclipse.egit.github.core.User;

import java.util.*;

public class DataBean extends AbstractDataBean<ConfigBean> {

    public static final String ISSUE_KEY_REGEX = "#([0-9]+)";

    private final ConfigBean configBean;
    private final GithubImportProcessBean importProcessBean;
    private final GithubIssueConverter converter;

    public DataBean(ConfigBean configBean, GithubImportProcessBean importProcessBean) {
        super(configBean);
        this.configBean = configBean;
        this.importProcessBean = importProcessBean;
        this.converter = new GithubIssueConverter(configBean, ComponentAccessor.getConstantsManager(),
                ComponentAccessor.getWorkflowManager(), ComponentAccessor.getWorkflowSchemeManager());
    }

    @Override
    public Set<ExternalUser> getRequiredUsers(Collection<ExternalProject> projects, ImportLogger importLogger) {
        return getAllUsers(importLogger);
    }

    @Override
    public Set<ExternalUser> getAllUsers(ImportLogger log) {
        Set<ExternalUser> users = new HashSet<ExternalUser>();
        for( Issue issue : configBean.getGithubDataService().getAllIssues() ) {
            users.add(convertUser(issue.getUser()));

            if( issue.getAssignee() != null ) {
                users.add(convertUser(issue.getAssignee()));
            }

            List<Comment> comments = configBean.getGithubDataService().getComments(issue);
            for( Comment comment : comments ) {
                users.add(convertUser(comment.getUser()));
            }
        }
        return users;
    }

    private ExternalUser convertUser(User user) {
        return new ExternalUser(user.getLogin(), user.getLogin());
    }

    @Override
    public Set<ExternalProject> getAllProjects(ImportLogger log) {
        return Sets.newHashSet(Iterables.transform(configBean.getGithubDataService().getProjects(), new Function<Project, ExternalProject>() {
            @Override
            public ExternalProject apply(Project project) {
                ExternalProject externalProject = new ExternalProject();
                String externalName = project.getRepository().generateId();

                externalProject.setKey(configBean.getProjectKey(externalName));
                externalProject.setName(configBean.getProjectName(externalName));
                externalProject.setLead(configBean.getProjectLead(externalName));

                externalProject.setId(project.getRepository().generateId());
                externalProject.setExternalName(externalName);
                externalProject.setDescription(project.getRepository().getDescription());
                externalProject.setUrl(project.getRepository().getHtmlUrl());

                externalProject.setWorkflowSchemeName(configBean.getSchemeStatusMapping().getWorkflowSchemeName());

                return externalProject;
            }
        }));
    }

    @Override
    public Iterator<ExternalIssue> getIssuesIterator(ExternalProject externalProject, final ImportLogger importLogger) {
        final Project project = configBean.getGithubDataService().getProjectByName(externalProject.getExternalName());

        return new Iterator<ExternalIssue>() {
            Iterator<Issue> issueIterator = project.getIssues().iterator();
            ExternalIssue next = null;

            @Override
            public boolean hasNext() {
                while( issueIterator.hasNext() && next == null) {
                    Issue githubIssue = issueIterator.next();
                    try {
                        next = converter.convertIssue(project, githubIssue, importLogger);
                    } catch (Exception e) {
                        importLogger.fail(e, "Failed convert issue #"+githubIssue.getNumber());
                    }
                }
                return next != null;
            }

            @Override
            public ExternalIssue next() {
                ExternalIssue externalIssue = next;
                next = null;
                return externalIssue;
            }

            @Override
            public void remove() {
                issueIterator.remove();
            }
        };
    }

    @Override
    public Collection<ExternalLink> getLinks(ImportLogger log) {
        return Lists.newArrayList();
    }

    @Override
    public long getTotalIssues(Set<ExternalProject> selectedProjects, ImportLogger log) {
        return configBean.getGithubDataService().getAllIssues().size();
    }

    @Override
    public String getUnusedUsersGroup() {
        return "github_import_unused";
    }

    @Override
    public void cleanUp() {
        // depending on the importProcessBean here is very ugly coupling -
        // but there is no other way to free data because this importProcessBean is not removed from the session by JIM automatically
        importProcessBean.cleanup();
    }

    @Override
    public String getIssueKeyRegex() {
        return ISSUE_KEY_REGEX;
    }

    @Override
    public Collection<ExternalVersion> getVersions(ExternalProject externalProject, ImportLogger importLogger) {
        final Project project = configBean.getGithubDataService().getProjectByName(externalProject.getExternalName());
        List<ExternalVersion> externalVersions = new ArrayList<ExternalVersion>();

        for(Milestone milestone : project.getMilestones()) {
            ExternalVersion version = new ExternalVersion();
            version.setName(milestone.getTitle());
            version.setDescription(milestone.getDescription());
            version.setReleaseDate(milestone.getDueOn());
            externalVersions.add(version);
        }

        return externalVersions;
    }

    @Override
    public Collection<ExternalComponent> getComponents(ExternalProject externalProject, ImportLogger importLogger) {
        return Collections.emptyList();
    }

    @Override
    public Collection<ExternalAttachment> getAttachmentsForIssue(ExternalIssue externalIssue, ImportLogger importLogger) {
        return converter.downloadAttachmentsForIssue(externalIssue, importLogger);
    }
}
