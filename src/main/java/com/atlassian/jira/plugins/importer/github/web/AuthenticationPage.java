package com.atlassian.jira.plugins.importer.github.web;

import com.atlassian.jira.plugins.importer.github.GithubImportProcessBean;
import com.atlassian.jira.plugins.importer.github.fetch.GithubAuthenticationService;
import com.atlassian.jira.plugins.importer.tracking.UsageTrackingService;
import com.atlassian.jira.plugins.importer.web.AbstractSetupPage;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.sal.api.ApplicationProperties;
import webwork.action.ActionContext;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * GitHub importer authentication page.
 */
public class AuthenticationPage extends AbstractSetupPage {

    public static final String SESSION_OAUTH_XSRF_TOKEN = "SESSION_OAUTH_XSRF_TOKEN";

    private final GithubAuthenticationService githubAuthenticationService;

    private final String redirectUrl;

    public AuthenticationPage(UsageTrackingService usageTrackingService, WebInterfaceManager webInterfaceManager, PluginAccessor pluginAccessor, ApplicationProperties applicationProperties, GithubAuthenticationService githubAuthenticationService) {
        super(usageTrackingService, webInterfaceManager, pluginAccessor);
        this.githubAuthenticationService = githubAuthenticationService;

        String jiraUrl = applicationProperties.getBaseUrl();
        try {
            redirectUrl = GithubAuthenticationService.REDIRECT_PAGE + "?jiraUrl=" + URLEncoder.encode(jiraUrl, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String doDefault() throws Exception {
        if (!isAdministrator()) {
            return "denied";
        }

        if (getController() == null) {
            return RESTART_NEEDED;
        }

        return INPUT;
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        if( isNextClicked() ) {
            try {
                GithubImportProcessBean processBean = (GithubImportProcessBean) getController().getImportProcessBeanFromSession();

                // only create new process if none existed (prevents having multiple parallel data fetch)
                if( processBean == null ) {
                    getController().createImportProcessBean(this);
                }
            } catch (ClassCastException e) {
                // might happen after updating the plugin if this class has changed
                log.warn("Unable to clean up old process bean", e);
                getController().createImportProcessBean(this);
            }

            GithubAuthenticationService.OauthRequest oauthRequest = githubAuthenticationService.getOauthUrl(redirectUrl);
            ActionContext.getSession().put(SESSION_OAUTH_XSRF_TOKEN, oauthRequest.xsrfToken);
            forceRedirect(oauthRequest.url);
            return NONE;
        } else {
            return super.doExecute();
        }
    }

    @Override
    public String getFormTitle() {
        return getText("com.atlassian.jira.plugins.importer.github.oauth.title");
    }
}
