package com.atlassian.jira.plugins.importer.github.fetch;

import com.atlassian.jira.plugins.importer.github.util.HttpClientFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.ProxySelectorRoutePlanner;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.net.ProxySelector;
import java.util.UUID;

/**
 * GitHub OAuth authentication.
 * <p>
 * OAuth for GitHub had to be implemented in a special way. During OAuth the application sends a redirect_url to GitHub which should bring
 * the client back to the local JIRA instance. However, GitHub enforces that this URL matches a pre-configured domain.
 * Since the host is different for every JIRA instance this does not work for us. We work around this issue by first redirecting the user
 * to another page which is at a fixed location ({@link #REDIRECT_PAGE}) and then redirect from there to the actual JIRA instance.
 */
public class GithubAuthenticationService {

    private final Logger log = Logger.getLogger(this.getClass());

    public final static String CLIENT_ID = "10d8eb7b4fd8e15cf550";

    // storing the client secret here is not great - but not a real security problem either
    private final static String CLIENT_SECRET = "490d6cf9b02498e197528d7bf09cdd0140edd8a7";

    public static final String REDIRECT_PAGE = "http://jira-importer.github.com/redirect.html";


    public AccessTokenResponse retrievePermanentAccessToken(String temporaryAccessCode) {
        AccessTokenResponse accessTokenResponse;

        String url = UriBuilder.fromPath("https://github.com/login/oauth/access_token")
                .queryParam("client_id", CLIENT_ID)
                .queryParam("client_secret", CLIENT_SECRET)
                .queryParam("code", temporaryAccessCode)
                .build().toString();

        try {
            HttpGet request = new HttpGet(url);
            request.setHeader("Accept", MediaType.APPLICATION_JSON);
            HttpResponse response = HttpClientFactory.createHttpClient().execute(request);

            ObjectMapper mapper = new ObjectMapper();
            DeserializationConfig deserializationConfig = mapper.getDeserializationConfig();
            accessTokenResponse = mapper.readValue(response.getEntity().getContent(), AccessTokenResponse.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // TODO more beautiful implementation with jersey-client. doesn't work do to classpath trouble
        // if the jersey-client is added (https://bitbucket.org/MarkusKramer/jira-importers-github-plugin/src/1280edf5f1ddba805ceae5dc06d3798eb12a98a6/pom.xml)
        // the rest services (jersey-server) break
//        ClientResponse response = restClient.resource("https://github.com/login/oauth/access_token")
//                .queryParam("client_id", CLIENT_ID)
//                .queryParam("client_secret", CLIENT_SECRET)
//                .queryParam("code", temporaryAccessCode)
//                .accept(MediaType.APPLICATION_JSON)
//                .get(ClientResponse.class);
//        if (response.getStatus() != ClientResponse.Status.OK.getStatusCode()) {
//            throw new RuntimeException("HTTP error code: " + response.getStatus());
//        }
//        accessTokenResponse = response.getEntity(new GenericType<AccessTokenResponse>(){});

        //System.out.println("retrievePermanentAccessToken response: " + accessTokenResponse);
        return accessTokenResponse;
    }

    public OauthRequest getOauthUrl(String redirectUrl) {
        String xsrfToken = UUID.randomUUID().toString();
        String url = UriBuilder.fromPath("https://github.com/login/oauth/authorize")
            .queryParam("client_id", CLIENT_ID)
            .queryParam("scope", "repo")
            .queryParam("state", xsrfToken)
            .queryParam("redirect_uri", redirectUrl)
            .build().toString();
        return new OauthRequest(url, xsrfToken);
    }

    @XmlRootElement
    public static class AccessTokenResponse {
        public String access_token;
        public String token_type;
        public String error;

        @Override
        public String toString() {
            return "AccessTokenResponse{" +
                    "access_token='" + access_token + '\'' +
                    ", token_type='" + token_type + '\'' +
                    ", error='" + error + '\'' +
                    '}';
        }
    }

    public static class OauthRequest {
        public String url;
        public String xsrfToken;

        public OauthRequest(String url, String xsrfToken) {
            this.url = url;
            this.xsrfToken = xsrfToken;
        }
    }
}
