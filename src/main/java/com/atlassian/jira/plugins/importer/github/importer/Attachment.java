package com.atlassian.jira.plugins.importer.github.importer;

public final class Attachment {
    private String filename;
    private String downloadUrl;

    public Attachment(String filename, String downloadUrl) {
        this.filename = filename;
        this.downloadUrl = downloadUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Attachment that = (Attachment) o;

        if (!downloadUrl.equals(that.downloadUrl)) return false;
        if (!filename.equals(that.filename)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = filename.hashCode();
        result = 31 * result + downloadUrl.hashCode();
        return result;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }
}
