package com.atlassian.jira.plugins.importer.github.web;

import com.atlassian.jira.plugins.importer.github.GithubImportProcessBean;
import com.atlassian.jira.plugins.importer.github.config.ConfigBean;
import com.atlassian.jira.plugins.importer.github.fetch.GithubAuthenticationService;
import com.atlassian.jira.plugins.importer.tracking.UsageTrackingService;
import com.atlassian.jira.plugins.importer.web.AbstractSetupPage;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.WebInterfaceManager;
import webwork.action.ActionContext;
import webwork.action.ServletActionContext;

import javax.servlet.http.HttpServletRequest;

/**
 * This pages receives the OAuth code from GitHub and obtains a permanent access token.
 */
public class ReceiveOauthCodePage extends AbstractSetupPage {

    private final GithubAuthenticationService githubAuthenticationService;

    public ReceiveOauthCodePage(UsageTrackingService usageTrackingService, WebInterfaceManager webInterfaceManager, PluginAccessor pluginAccessor, GithubAuthenticationService githubAuthenticationService) {
        super(usageTrackingService, webInterfaceManager, pluginAccessor);
        this.githubAuthenticationService = githubAuthenticationService;
    }

    @Override
    public String doDefault() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();

        String code = request.getParameter("code");
        if( code != null ) {
            // verify xsrf token
            String xsrfToken = request.getParameter("state");
            String correctXsrfToken = (String) ActionContext.getSession().get(AuthenticationPage.SESSION_OAUTH_XSRF_TOKEN);
            if( correctXsrfToken.equals(xsrfToken) ) {
                GithubAuthenticationService.AccessTokenResponse accessTokenResponse = githubAuthenticationService.retrievePermanentAccessToken(code);
                if( accessTokenResponse.error == null ) {
                    getConfigBean().getGithubDataService().setAccessToken(accessTokenResponse.access_token);
                } else {
                    addError("accessToken", getText("com.atlassian.jira.plugins.importer.github.receiveOauthCode.error.github", accessTokenResponse.error));
                }
            } else {
                addError("accessToken", getText("com.atlassian.jira.plugins.importer.github.receiveOauthCode.error.xsrfIncorrect"));
            }
        } else {
            addError("accessToken", getText("com.atlassian.jira.plugins.importer.github.receiveOauthCode.error.userDeniedAccess"));
        }

        return INPUT;
    }

    private ConfigBean getConfigBean() {
        return (ConfigBean) getProcessBean().getConfigBean();
    }

    private GithubImportProcessBean getProcessBean() {
        try {
            if (getController() == null || getController().getImportProcessBeanFromSession() == null) {
                throw new IllegalStateException("Importer not intialized");
            }
            return (GithubImportProcessBean) getController().getImportProcessBeanFromSession();
        } catch (ClassCastException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getFormTitle() {
        return getText("com.atlassian.jira.plugins.importer.github.receiveOauthCode.title");
    }

}
