package com.atlassian.jira.plugins.importer.github.config;

import java.util.HashMap;
import java.util.Map;

/**
 * Issue status mappings for the selected schema.
 */
public class SchemeStatusMapping {

    /** null indicates the default scheme */
    private String workflowSchemeName;
    private Map<String, JiraStatusMapping> workflowIdToStatusMapping;

    public SchemeStatusMapping(String workflowSchemeName) {
        this.workflowSchemeName = workflowSchemeName;
        this.workflowIdToStatusMapping = new HashMap<String, JiraStatusMapping>();
    }

    public String getWorkflowSchemeName() {
        return workflowSchemeName;
    }

    public void setWorkflowSchemeName(String workflowSchemeName) {
        this.workflowSchemeName = workflowSchemeName;
    }

    public Map<String, JiraStatusMapping> getWorkflowIdToStatusMapping() {
        return workflowIdToStatusMapping;
    }

    public void setWorkflowIdToStatusMapping(Map<String, JiraStatusMapping> workflowIdToStatusMapping) {
        this.workflowIdToStatusMapping = workflowIdToStatusMapping;
    }

    /**
     * To what the GitHub issue state will be mapped to. Depends on workflow
     */
    public static class JiraStatusMapping {
        private String openStatus;
        private String closedStatus;

        public JiraStatusMapping() {
        }

        public JiraStatusMapping(String openStatus, String closedStatus) {
            this.openStatus = openStatus;
            this.closedStatus = closedStatus;
        }

        public String getOpenStatus() {
            return openStatus;
        }

        public void setOpenStatus(String openStatus) {
            this.openStatus = openStatus;
        }

        public String getClosedStatus() {
            return closedStatus;
        }

        public void setClosedStatus(String closedStatus) {
            this.closedStatus = closedStatus;
        }
    }

}
