package com.atlassian.jira.plugins.importer.github.rest;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugins.importer.extensions.ExternalSystemImporterModuleDescriptor;
import com.atlassian.jira.plugins.importer.github.GithubImportProcessBean;
import com.atlassian.jira.plugins.importer.github.GithubImporterController;
import com.atlassian.jira.plugins.importer.github.fetch.DataFetchJob;
import com.atlassian.jira.plugins.importer.github.fetch.FetchProgress;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.Permissions;
import com.atlassian.plugin.PluginAccessor;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * REST service for fetching data from Github.
 */
@Path("fetchData")
public class FetchDataResource {

    private final Logger log = Logger.getLogger(this.getClass());

    private final JiraAuthenticationContext authenticationContext;
    private final GlobalPermissionManager globalPermissionManager;
    private final PluginAccessor pluginAccessor;

    public FetchDataResource(JiraAuthenticationContext authenticationContext, GlobalPermissionManager globalPermissionManager,
                             PluginAccessor pluginAccessor) {
        this.authenticationContext = authenticationContext;
        this.globalPermissionManager = globalPermissionManager;
        this.pluginAccessor = pluginAccessor;
    }

    @GET
    @Path("/{externalSystem}/progress")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getProgress(@Context HttpServletRequest req, @PathParam("externalSystem") String externalSystem) {
        if (!isAdministrator()) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        log.debug("Returning data fetch progress");

        HttpSession session = req.getSession(false);
        GithubImporterController controller = getController(externalSystem);
        if (session == null || controller == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("No active importer found").build();
        }

        GithubImportProcessBean githubImportProcessBean = (GithubImportProcessBean) controller.getImportProcessBean(session);
        if( githubImportProcessBean != null && githubImportProcessBean.getDataFetchJob() != null ) {
            DataFetchJob dataFetchJob = githubImportProcessBean.getDataFetchJob();
            String json = toJSON(dataFetchJob.getFetchProgress());
            return Response.ok(json).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity("No data fetch operation has been started").build();
        }
    }

    @POST
    @Path("/{externalSystem}/cancel")
    public Response cancel(@Context HttpServletRequest req, @PathParam("externalSystem") String externalSystem) {
        if (!isAdministrator()) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        log.debug("Canceling data fetch");

        HttpSession session = req.getSession(false);
        GithubImporterController controller = getController(externalSystem);
        if (session == null || controller == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("No active importer found").build();
        }

        GithubImportProcessBean githubImportProcessBean = (GithubImportProcessBean) controller.getImportProcessBean(session);
        if( githubImportProcessBean != null && githubImportProcessBean.getDataFetchJob() != null ) {
            DataFetchJob dataFetchJob = githubImportProcessBean.getDataFetchJob();
            dataFetchJob.cancel();
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity("No data fetch operation has been started").build();
        }
    }


    protected boolean isAdministrator() {
        User currentUser = authenticationContext.getLoggedInUser();
        return (currentUser != null) && globalPermissionManager.hasPermission(Permissions.ADMINISTER, currentUser);
    }

    private GithubImporterController getController(String externalSystem) {
        ExternalSystemImporterModuleDescriptor moduleDescriptor = (ExternalSystemImporterModuleDescriptor) pluginAccessor.getEnabledPluginModule(externalSystem);
        if( moduleDescriptor == null ) {
            throw new RuntimeException("No active ImporterController found");
        }
        return (GithubImporterController) moduleDescriptor.getModule();
    }

    private String toJSON(FetchProgress fetchProgress) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(fetchProgress);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
