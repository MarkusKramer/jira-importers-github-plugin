package com.atlassian.jira.plugins.importer.github.fetch;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Fetch progress information for the client.
 */
@XmlRootElement
public class FetchProgress {

    private int currentClosedIssue;
    private Integer totalClosedIssues;
    private int currentOpenIssue;
    private Integer totalOpenIssues;
    private String error;
    private String warning;

    public int getCurrentClosedIssue() {
        return currentClosedIssue;
    }

    public void setCurrentClosedIssue(int currentClosedIssue) {
        this.currentClosedIssue = currentClosedIssue;
    }

    public Integer getTotalClosedIssues() {
        return totalClosedIssues;
    }

    public void setTotalClosedIssues(Integer totalClosedIssues) {
        this.totalClosedIssues = totalClosedIssues;
    }

    public int getCurrentOpenIssue() {
        return currentOpenIssue;
    }

    public void setCurrentOpenIssue(int currentOpenIssue) {
        this.currentOpenIssue = currentOpenIssue;
    }

    public Integer getTotalOpenIssues() {
        return totalOpenIssues;
    }

    public void setTotalOpenIssues(Integer totalOpenIssues) {
        this.totalOpenIssues = totalOpenIssues;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getWarning() {
        return warning;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }
}
