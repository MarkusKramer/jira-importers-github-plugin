package com.atlassian.jira.plugins.importer.github.rest.workflow;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * REST service to access workflow information.
 */
@Path("workflow")
public class WorkflowResource {

    private final Logger log = Logger.getLogger(this.getClass());

    /** Used internally to identify the default workflow scheme. In service calls null is used to access the default scheme. */
    public static final String DEFAULT_WORKFLOW_SCHEME_KEY = "[DEFAULT-WORKFLOW-SCHEME]";

    private final JiraAuthenticationContext authenticationContext;
    private final GlobalPermissionManager globalPermissionManager;
    private final PluginAccessor pluginAccessor;
    private final WorkflowManager workflowManager;
    private final WorkflowSchemeManager workflowSchemeManager;
    private final ConstantsManager constantsManager;
    private final WorkflowService workflowService;

    public WorkflowResource(JiraAuthenticationContext authenticationContext, GlobalPermissionManager globalPermissionManager, PluginAccessor pluginAccessor, WorkflowManager workflowManager, WorkflowSchemeManager workflowSchemeManager, ConstantsManager constantsManager) {
        this.authenticationContext = authenticationContext;
        this.globalPermissionManager = globalPermissionManager;
        this.pluginAccessor = pluginAccessor;
        this.workflowManager = workflowManager;
        this.workflowSchemeManager = workflowSchemeManager;
        this.constantsManager = constantsManager;
        workflowService = new WorkflowService(workflowManager, workflowSchemeManager, constantsManager);
    }

    /**
     * Returns a list of {@link SchemeWorkflow}s as JSON.
     */
    @GET
    @Path("/scheme")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSchemeWorkflows(@Context HttpServletRequest req, @QueryParam("name") String workflowSchemeName) {
        if (!isAdministrator()) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        log.debug("Retrieving states of workflow scheme "+workflowSchemeName);

        if( DEFAULT_WORKFLOW_SCHEME_KEY.equals(workflowSchemeName) ) {
            workflowSchemeName = null;
        }

        Map<String,SchemeWorkflow> workflowToSchemeWorkflowMap = new HashMap<String, SchemeWorkflow>();

        for( IssueType issueType : constantsManager.getAllIssueTypeObjects() ) {
            JiraWorkflow workflow = workflowService.getWorkflow(workflowSchemeName, issueType.getId());

            SchemeWorkflow schemeWorkflow = workflowToSchemeWorkflowMap.get(workflow.getName());
            if( schemeWorkflow == null ) {
                schemeWorkflow = new SchemeWorkflow(workflow.getName(), workflow.getDescription());
                workflowToSchemeWorkflowMap.put(workflow.getName(), schemeWorkflow);
            }

            schemeWorkflow.getAssociatedTypes().add(new SchemeWorkflow.JiraIssueType(issueType.getId(),issueType.getName(),issueType.getIconUrl()));

            List<SchemeWorkflow.JiraIssueStatus> statuses = Lists.transform(workflow.getLinkedStatusObjects(), new Function<Status, SchemeWorkflow.JiraIssueStatus>() {
                @Override
                public SchemeWorkflow.JiraIssueStatus apply(@Nullable Status input) {
                    return new SchemeWorkflow.JiraIssueStatus(input.getId(), input.getName());
                }
            });
            schemeWorkflow.setStatuses(statuses);
        }

        String json = toJSON(workflowToSchemeWorkflowMap.values());
        return Response.ok(json).build();
    }

    protected boolean isAdministrator() {
        User currentUser = authenticationContext.getLoggedInUser();
        return (currentUser != null) && globalPermissionManager.hasPermission(Permissions.ADMINISTER, currentUser);
    }

    private String toJSON(Object obj) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(obj);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
