package it.com.atlassian.jira.plugins.importer.github.web.po;

import org.junit.Assert;

public class ImporterProjectsMappingsPage extends com.atlassian.jira.plugins.importer.po.common.ImporterProjectsMappingsPage {

    /**
     * Our next page is different then in other JIM importers.
     */
    public LabelMappingPage next2() {
        Assert.assertTrue("Next button is disabled", nextButton.isEnabled());
        nextButton.click();
        return pageBinder.bind(LabelMappingPage.class);
    }
}
