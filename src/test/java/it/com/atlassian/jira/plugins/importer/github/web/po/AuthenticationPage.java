package it.com.atlassian.jira.plugins.importer.github.web.po;

import com.atlassian.jira.plugins.importer.po.common.AbstractImporterWizardPage;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AuthenticationPage extends AbstractImporterWizardPage {

    @FindBy(id = "code")
    WebElement code;

    @Override
    public String getUrl() {
        return "/secure/admin/views/AuthenticationPage!default.jspa?externalSystem=" +
                "com.atlassian.jira.plugins.jira-importers-github-plugin:githubImporterKey";
    }

    public void next() {
        Assert.assertTrue(nextButton.isEnabled());
        nextButton.click();
    }

    /**
     * Only get a handle on the next wizard page.
     */
    public ImporterProjectsMappingsPage getNext() {
        return pageBinder.bind(ImporterProjectsMappingsPage.class);
    }

}
