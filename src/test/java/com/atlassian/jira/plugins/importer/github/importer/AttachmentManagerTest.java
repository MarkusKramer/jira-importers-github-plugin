package com.atlassian.jira.plugins.importer.github.importer;

import junit.framework.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class AttachmentManagerTest {

    @Test
    public void testConvertion() {
        AttachmentManager attachmentManager = new AttachmentManager();
        String input, output;
        Set<Attachment> attachments;

        // multiple links
        input = "link 1 [a1](http://example.com/a1.jpg) link 2 [a2](https://example.com/a2.jpg)";
        attachments = new HashSet<Attachment>();
        output = attachmentManager.convertMarkdownLinks(input, attachments);
        Assert.assertEquals("link 1 http://example.com/a1.jpg link 2 https://example.com/a2.jpg", output);
        Assert.assertEquals(0, attachments.size());

        // attachments
        input = "![landscape_7](https://f.cloud.github.com/assets/1502825/389403/746768ca-a706-11e2-829c-b88fb39fb0b8.jpg)";
        attachments = new HashSet<Attachment>();
        output = attachmentManager.convertMarkdownLinks(input, attachments);
        Assert.assertEquals("!landscape_7.jpg|thumbnail!", output);
        Assert.assertEquals(1, attachments.size());
        Assert.assertEquals("landscape_7.jpg", attachments.iterator().next().getFilename());
        Assert.assertEquals("https://f.cloud.github.com/assets/1502825/389403/746768ca-a706-11e2-829c-b88fb39fb0b8.jpg", attachments.iterator().next().getDownloadUrl());

        // duplicate attachments
        input = "![a1](https://f.cloud.github.com/assets/a1.jpg) ![a1](https://f.cloud.github.com/assets/a1.jpg)";
        attachments = new HashSet<Attachment>();
        output = attachmentManager.convertMarkdownLinks(input, attachments);
        Assert.assertEquals("!a1.jpg|thumbnail! !a1.jpg|thumbnail!", output);
        Assert.assertEquals(1, attachments.size());
        Assert.assertEquals("a1.jpg", attachments.iterator().next().getFilename());
        Assert.assertEquals("https://f.cloud.github.com/assets/a1.jpg", attachments.iterator().next().getDownloadUrl());

        // multiple links
        input = "link 1 [a1](http://example.com/a1.jpg \"bla bla\")";
        attachments = new HashSet<Attachment>();
        output = attachmentManager.convertMarkdownLinks(input, attachments);
        Assert.assertEquals("link 1 http://example.com/a1.jpg", output);
        Assert.assertEquals(0, attachments.size());
    }

}
